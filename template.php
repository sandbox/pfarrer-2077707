<?php

/**
 * Add Bootstrap3's meta tag into HTML head.
 */
function bootstrap3_preprocess_html(&$vars) {
	// Add meta-tag: <meta name="viewport" content="width=device-width, initial-scale=1.0">
	$element = array(
		'#tag' => 'meta',
		'#attributes' => array(
			'name' => 'viewport',
			'content' => 'width=device-width, initial-scale=1.0'
		)
	);
	drupal_add_html_head($element, 'bootstrap3_meta_viewport');
}

/**
 * Add bootstrap specific classes to all tables.
 */
function bootstrap3_preprocess_table(&$variables, $hook) {
	$classes = array('table', 'table-striped', 'table-hover');

	if (!isset($variables['attributes']['class'])) {
		$variables['attributes']['class'] = $classes;
	}
	else if (is_array($variables['attributes']['class'])) {
		$variables['attributes']['class'] = array_merge($variables['attributes']['class'], $classes);
	}
	else {
		$variables['attributes']['class'] = array_merge(array($variables['attributes']['class']), $classes);
	}
}

/**
 * Set different tpl file for blocks that are displayed in
 * specific regions.
 */
function bootstrap3_preprocess_block(&$variables, $hook) {
	$regions_without_block_panel = array(
		'header',
		'footer_first', 'footer_second',
		'footer_left', 'footer_center', 'footer_right',
	);

	if (in_array($variables['elements']['#block']->region, $regions_without_block_panel)) {
		$variables['theme_hook_suggestions'][] = 'block__no_panel';
	}
}

/**
 * Remove some internal css files.
 */
function bootstrap3_css_alter(&$css) {
	/*
		'misc/vertical-tabs.css' => FALSE,
		'modules/aggregator/aggregator.css' => FALSE,
		'modules/block/block.css' => FALSE,
		'modules/book/book.css' => FALSE,
		'modules/comment/comment.css' => FALSE,
		'modules/dblog/dblog.css' => FALSE,
		'modules/file/file.css' => FALSE,
		'modules/filter/filter.css' => FALSE,
		'modules/forum/forum.css' => FALSE,
		'modules/help/help.css' => FALSE,
		'modules/menu/menu.css' => FALSE,
		'modules/node/node.css' => FALSE,
		'modules/openid/openid.css' => FALSE,
		'modules/poll/poll.css' => FALSE,
		'modules/profile/profile.css' => FALSE,
		'modules/search/search.css' => FALSE,
		'modules/statistics/statistics.css' => FALSE,
		'modules/syslog/syslog.css' => FALSE,
		'modules/system/admin.css' => FALSE,
		'modules/system/maintenance.css' => FALSE,
		'modules/system/system.css' => FALSE,
		'modules/system/system.admin.css' => FALSE,
		'modules/system/system.base.css' => FALSE,
		'modules/system/system.maintenance.css' => FALSE,
		'modules/system/system.menus.css' => FALSE,
		'modules/system/system.messages.css' => FALSE,
	 	'modules/taxonomy/taxonomy.css' => FALSE,
		'modules/tracker/tracker.css' => FALSE,
		'modules/update/update.css' => FALSE,
		'modules/user/user.css' => FALSE,
	 */
	$exclude = array(
		'modules/system/system.theme.css' => FALSE,
	);
	$css = array_diff_key($css, $exclude);
}

/**
 * Reformat breadcrumbs.
 */
function bootstrap3_breadcrumb($vars) {
	if (empty($vars['breadcrumb'])) return '';

	$html = '<ol class="breadcrumb">';
	foreach ($vars['breadcrumb'] as $crumb) {
		$html .= '<li>'.$crumb.'</li>';
	}

	$html .= '</ol>';
	return $html;
}

/**
 * Format Novigation links.
 */
function bootstrap3_links__system_navigation($vars) {
	$html = '<ul '.drupal_attributes($vars['attributes']).'>';

	foreach ($vars['links'] as $link) {
		$html .= '<li>'.l($link['title'], $link['href']).'</li>';
	}

	return $html.'</ul>';
}
function bootstrap3_menu_local_tasks(&$vars) {
	$output = '';

	if (!empty($vars['primary'])) {
		$vars['primary']['#prefix'] = '<ul class="nav nav-tabs">';
		$vars['primary']['#suffix'] = '</ul>';
		$output .= drupal_render($vars['primary']);
	}
	if (!empty($vars['secondary'])) {
		$vars['secondary']['#prefix'] = '<ul class="nav nav-pills">';
		$vars['secondary']['#suffix'] = '</ul>';
		$output .= drupal_render($vars['secondary']);
	}

	return $output;
}

/**
 * Reformat system messages.
 */
function bootstrap3_status_messages($variables) {
	$display = $variables['display'];
	$output = '';

	foreach (drupal_get_messages($display) as $type => $messages) {
		// Translate between Drupal message type names and Bootstrap names
		switch ($type) {
			case 'status': $bootstrap_type = 'info'; break;
			case 'error': $bootstrap_type = 'danger'; break;
			default:
				// Only in case of 'warning'
				$bootstrap_type = $type; break;
		}

		foreach ($messages as $msg) {
			$output .= '<div class="alert alert-'.$bootstrap_type.'">'.$msg.'</div>';
		}
	}

	return $output;
}

