<?php

function bootstrap3_form_system_theme_settings_alter(&$form, &$form_state) {

	$form['specific'] = array(
		'#type' => 'fieldset',
		'#title' => t('Bootstrap-specifiy options'),
		'#weight' => 5,
		'#collapsible' => false,
	);

	$form['specific']['navbar_inverse'] = array(
		'#type' => 'checkbox',
		'#title' => t('Navbar Inverse style'),
		'#default_value' => theme_get_setting('navbar_inverse'),
		'#description' => t('Specify whether the navigation bar should use the inverse (dark) style or the bright gray style.'),
	);
}