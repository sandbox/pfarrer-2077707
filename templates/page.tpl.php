<section id="navbar" class="navbar <?php print theme_get_setting('navbar_inverse') ? 'navbar-inverse' : 'navbar-default'; ?> navbar-fixed-top <?php print $classes; ?>" role="navigation">
	<div class="container">

		<?php if ($site_name): ?>
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php print $front_page; ?>" rel="home">
				<?php print $site_name; ?>
			</a>
		</div>
		<?php endif; ?>

		<div class="collapse navbar-collapse">
			<?php print theme('links__system_navigation', array('links' => $main_menu, 'attributes' => array('class' => array('nav', 'navbar-nav')))); ?>
			<?php print theme('links__system_navigation', array('links' => $secondary_menu, 'attributes' => array('class' => array('nav', 'navbar-nav', 'navbar-right')))); ?>
		</div>

	</div>
</section>

<div id="page-wrapper">
	<section id="header">

		<header class="container">

			<?php if ($logo): ?>
			<div class="row">
				<div class="col-md-12">

					<?php if ($site_name || $site_slogan): ?>
					<div id="name-and-slogan" class="pull-left">
						<?php if ($site_name): ?>
						<h1 id="site-name">
							<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
								<span><?php print $site_name; ?></span>
							</a>
						</h1>
						<?php endif; ?>

						<?php if ($site_slogan): ?>
						<div id="site-slogan">
							<?php print $site_slogan; ?>
						</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>

					<div id="logo_wrapper" class="pull-right">
						<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
							<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
						</a>
					</div>

				</div>
			</div>
			<?php endif; ?>

			<div class="row">
				<div class="col-md-12"><?php print render($page['header']); ?></div>
			</div>

		</header>

	</section>

	<?php if ($messages): ?>
	<section id="messages" class="container">
		<div class="row">
			<div class="col-md-12"><?php print $messages; ?></div>
		</div>
	</section>
	<?php endif; ?>

	<section id="content" class="container">

		<?php if ($breadcrumb): ?>
		<div id="breadcrumb" class="row">
			<div class="col-md-12"><?php print $breadcrumb; ?></div>
		</div>
		<?php endif; ?>

		<?php print render($title_prefix); ?>
		<?php if ($title): ?>
		<div class="title row">
			<h2 class="title col-md-12" id="page-title"><?php print $title; ?></h2>
		</div>
		<?php endif; ?>
		<?php print render($title_suffix); ?>

		<?php if ($tabs): ?>
		<div class="tabs row">
			<div class="col-md-12"><?php print render($tabs); ?></div>
		</div>
		<?php endif; ?>

		<div class="row">
			<?php if ($page['sidebar_first']): ?>
			<aside id="sidebar_first" class="col-md-3"><?php print render($page['sidebar_first']); ?></aside>
			<?php endif; ?>

			<div id="main_content" class="col-md-<?php echo 12-($page['sidebar_first'] ? 3 : 0)-($page['sidebar_second'] ? 3 : 0); ?>">
				<?php if ($action_links): ?>
				<ul class="action-links row">
					<div class="col-md-12"><?php print render($action_links); ?></div>
				</ul>
				<?php endif; ?>

				<?php print render($page['content']); ?>
			</div>

			<?php if ($page['sidebar_second']): ?>
			<aside id="sidebar_second" class="col-md-3"><?php print render($page['sidebar_second']); ?></aside>
			<?php endif; ?>
		</div>

	</section>

	<section id="footer" class="container">

		<?php if ($page['footer_first']): ?>
		<div id="footer-first" class="row">
			<div class="col-md-12"><?php print render($page['footer_first']); ?></div>
		</div>
		<?php endif; ?>

		<?php if ($page['footer_left'] || $page['footer_center'] || $page['footer_right']): ?>
		<div id="footer-divided" class="row">
			<div id="footer-left" class="col-md-4">
				<?php print render($page['footer_left']); ?>
			</div>
			<div id="footer-center" class="col-md-4">
				<?php print render($page['footer_center']); ?>
			</div>
			<div id="footer-right" class="col-md-4">
				<?php print render($page['footer_right']); ?>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($page['footer_second']): ?>
		<div id="footer-second" class="row">
			<div class="col-md-12"><?php print render($page['footer_second']); ?></div>
		</div>
		<?php endif; ?>

	</section>
</div>