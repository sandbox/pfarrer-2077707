<div id="<?php print $block_html_id; ?>" class="panel panel-default <?php print $classes; ?>"<?php print $attributes; ?>>
	<?php print render($title_prefix); ?>
	<?php if ($block->subject): ?>
	<div class="panel-heading"<?php print $title_attributes; ?>><?php print $block->subject ?></div>
	<?php endif;?>
	<?php print render($title_suffix); ?>

	<div class="content panel-body"<?php print $content_attributes; ?>>
		<?php print $content; ?>
	</div>
</div>
